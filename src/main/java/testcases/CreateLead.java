package testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import Mypages.LoginPage;
import wdMethods.ProjectMethods;

public class CreateLead extends ProjectMethods {
	@BeforeTest
	public void setdata()
	{
		System.out.println("");
		testCaseName = "Create_Lead";
		testDescription = "LoginAndLogout";
		authors = "sarath";
		category = "smoke";
		dataSheetName = "TC002_CreateLead";
		testNodes = "Leads";
	}

	@Test(dataProvider = "fetchData")
	public void login(String userName,String password,String cName, String sName,String lName) {		
		new LoginPage()
		.enterUserName(userName)
		.enterPassword(password)
		.clickLogin().clickcrmsfa().clickLeads().createLeads().getCompanyName(cName).getForename(sName).getSurname(lName);
				
		/*LoginPage lp = new LoginPage();
		lp.enterUserName();
		lp.enterPassword();
		lp.clickLogin();*/
	}
	
	
}
