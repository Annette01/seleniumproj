package Mypages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class myLeads extends ProjectMethods {

	public myLeads() {
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(linkText = "Create Lead") WebElement elecreateleads;
    public createLead createLeads()
    {
    	click(elecreateleads);
    	return new createLead();
    }
}
