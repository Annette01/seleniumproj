package Mypages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class myhomepage extends ProjectMethods {
	
	public myhomepage()
	{
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(linkText = "Leads") WebElement eleleads;
	public myLeads clickLeads()
	{
		click(eleleads);
		return new myLeads();
	}

}
