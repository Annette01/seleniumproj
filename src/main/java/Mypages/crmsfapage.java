package Mypages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class crmsfapage extends ProjectMethods {

	 public crmsfapage()
	 {
		 PageFactory.initElements(driver, this);
		 
	 }
	 @FindBy(how = How.LINK_TEXT, using = "CRM/SFA") WebElement eleclicksrmsfa; 
	 public myhomepage clickcrmsfa()
	 {
		 click(eleclicksrmsfa);
		 return new myhomepage();
		 
	 }
}
