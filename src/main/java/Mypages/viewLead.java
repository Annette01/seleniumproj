package Mypages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class viewLead extends ProjectMethods {

	public viewLead()
	{
		PageFactory.initElements(driver, this);
	}
	@FindBy(linkText = "Merge Leads") WebElement elemergeleads;
	public mergelead mergelead()
	{
		click(elemergeleads);
		return new mergelead();
	}
	
}
