package Mypages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.Test;

import wdMethods.ProjectMethods;

public class createLead extends ProjectMethods{

	public createLead()
	{
		PageFactory.initElements(driver, this);
	}
	 
	@FindBy(id = "createLeadForm_companyName") WebElement elecomname;
	public createLead getCompanyName(String data)
	{
		type(elecomname,data);
		return this;
	}
    @FindBy(id = "createLeadForm_firstName") WebElement eleforename;
	public createLead getForename(String data)
	{
		type(eleforename,data);
		return this;
	}
	 @FindBy(id = "createLeadForm_lastName") WebElement elesurname;
		public createLead getSurname(String data)
		{
			type(elesurname,data);
			return this;
		}
		 @FindBy(id = "createLeadForm_lastName") WebElement creatleadsubmit;
			public viewLead clicksubmit()
			{
				click(creatleadsubmit);
				return new viewLead();
			}
	
}
